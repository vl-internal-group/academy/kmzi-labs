
export default function permutation ( bin, scheme ) {
  return scheme.reduce(( acc, position ) => acc + bin[position - 1], '');
}

