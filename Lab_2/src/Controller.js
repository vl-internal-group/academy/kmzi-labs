import DES from './DES.js';

import converter from './helpers/converter.js';

export default class Controller {

  constructor() {
    this.DEScodding = new DES();
  }

  getDOMValue( id ) {
    return document.getElementById( id ).value
  }

  setDOMValue( id, value ) {
    document.getElementById( id ).value = value
  }

  convertIfExist( value ) {
    return value ? converter.str2bin( value ) : value;
  }

  encrypting() {
    try {
      let value = this.getDOMValue( "in-text" );
      let key = this.getDOMValue( "key-enc" );

      this.setDOMValue( "out-text",
        converter.bin2str( this.DEScodding.encrypt( this.convertIfExist(value), this.convertIfExist(key)) ));

    } catch ( e ) { console.error( e ) }
  }

  decrypting() {
    try {
      let value = this.getDOMValue( "out-text" );
      let key = this.getDOMValue( "key-decr" );

      let result = this.DEScodding.decrypt( converter.str2bin( value ) , this.convertIfExist( key ) );
      this.setDOMValue( "in-text", converter.bin2str( result ));

    } catch ( e ) { console.error( e ) }
  }

}