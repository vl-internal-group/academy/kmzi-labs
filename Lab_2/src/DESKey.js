import desTables from "./config/DEStables.js";

import permutation from "./helpers/permutation.js";
import chunkString from "./helpers/chunkString.js";

const SIZE_OF_KEY = 64;
const SUBKEYS_ROUND = 16;

export default class Key {
  constructor( key ){
    this.key = key ? this.refactorKey( key , SIZE_OF_KEY ) : this.generateRandomKey();
    this.roundKeys = [];
  }

  generateRandomKey() {
    let key = [];
    for ( let i = 0; i < 64; i++ ) {
      key.push( Math.random() > 0.5 ? '1' : '0' );
    }
    return key;
  }

  populateSubKeys( ) {

    // PC-1 permutation
    this.key = permutation( this.key, desTables.permutation.initKey );

    let subKeys = [ chunkString( this.key, this.key.length / 2 ) ]; // C - 0, D - 1

    for ( let i = 0; i < SUBKEYS_ROUND; i++ ) { // 56
      subKeys.push([
        this.performKeyLeftShift( subKeys[i][0], desTables.keyShift[i] ), // 28
        this.performKeyLeftShift( subKeys[i][1], desTables.keyShift[i] )  // 28
      ]);

      // PC-2 permutation
      this.roundKeys.push( permutation( subKeys[i + 1][0] + subKeys[i + 1][1], desTables.permutation.subKey ))
    }
  }

  performKeyLeftShift( key, shift) {
    return key.substr( shift ) + key.slice(0, shift )
  }

  // cuts or pad key to 64b
  refactorKey( input,  lengthKey ) {
    return input.length > lengthKey ? input.substr( 0, lengthKey ) : "0".repeat( lengthKey - input.length ) + input;
  }
}