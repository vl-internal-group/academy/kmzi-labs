// Best explanation ever :
// http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm

import Key from "./DESKey.js";
import converter from "./helpers/converter.js";
import permutation from "./helpers/permutation.js";
import desTables from "./config/DEStables.js";
import chunkString from "./helpers/chunkString.js";

const SIZE_OF_BLOCK = 64;

const QUANTITY_OF_ROUNDS = 16;
const S_BLOCK_COLUMNS = 16;
const S_BLOCK_CHUNK_PRE_SIZE = 6;
const S_BLOCK_CHUNK_POST_SIZE = 4;

export default class DES {
  constructor(){
    this.blocks = [];
  }

  encrypt( input, key ) {
    return this.desFeistelOperation( input, key, true )
  }

  refactorInputLength( input ) { // pad zeroes for n * 64b message
    let rep = input.length % SIZE_OF_BLOCK;
    return input + '0'.repeat( rep && ( SIZE_OF_BLOCK - rep ));
  }

  cutTextIntoBlocks( input ) { // cut to 64b blocks
    let blocks = [];

    let blocksCount = input.length / SIZE_OF_BLOCK;
    let lengthOfBlock = input.length / blocksCount;

    for ( let i = 0; i < blocksCount; i++ ) {
      blocks.push( input.substr( i * lengthOfBlock, lengthOfBlock ))
    }

    return blocks;
  }

  XOR( s1, s2 ) {
    // you cannot just convert to int and ^ XOR it. js removes padding zeroes and the result has been changed
    return [...s1].reduce(( acc, cur, i ) => acc + ( s1[i] ^ s2[i] ), '');
  }

  desFeistelOperation( input, key, isEncryption ) {

    this.blocks = [];
    if ( !input ) {
      throw Error( "Not enough parameters provided for decryption" )
    }

    this.desKey = new Key( key );

    this.desKey.populateSubKeys(); // creates round keys 1 -> QUANTITY_OF_ROUNDS

    this.blocks = this.cutTextIntoBlocks( this.refactorInputLength( input ), false );

    let result = [];
    for ( const block of this.blocks ) {
      // IP permutation
      let newBlock = permutation( block, desTables.permutation.initMessage );

      let subMessage = chunkString( newBlock, SIZE_OF_BLOCK / 2 ); // Left - 0, Right - 1

      // processed through all rounds
      for ( let i = 0; i < QUANTITY_OF_ROUNDS; i++ ) {
        let expandedRight = permutation( subMessage[1], desTables.messageExpansion ); // E(Rn-1). 32 -> 48
        let xored = this.XOR( this.desKey.roundKeys[ isEncryption? i :  QUANTITY_OF_ROUNDS - i - 1 ], expandedRight ); // Kn + E(Rn-1).

        // `S`-boxes. 6*8->4*8 = 32b
        let SChunks = chunkString( xored, S_BLOCK_CHUNK_PRE_SIZE );

        SChunks = SChunks.map(( el, it ) =>
          converter.num2bin(
            desTables.S[it][ parseInt(el[0]+el[el.length-1],2) * S_BLOCK_COLUMNS + parseInt(el.substr(1,el.length-2),2) ]
            , S_BLOCK_CHUNK_POST_SIZE )
        ).join('');

        // P permutations for chunks
        let f = permutation( SChunks, desTables.permutation.rightMessage );

        subMessage = [ subMessage[ 1 ], this.XOR( subMessage[ 0 ], f ) ];

      }

      // IP-1 Final inverse permutation
      result.push( permutation ( subMessage[1] + subMessage[0], desTables.permutation.finalMessage ));

    }
    return result.join('');
  }

  decrypt( input, key ) {
    return this.desFeistelOperation( input, key, false )
  }


}

