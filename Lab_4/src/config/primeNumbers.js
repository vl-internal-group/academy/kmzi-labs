import getPrimeInRange from '../helpers/getPrimeInRange.js';

const PRIME_RANGE_BEFORE = 50;
const PRIME_RANGE_AFTER = 200;

export default getPrimeInRange(PRIME_RANGE_AFTER).filter( el => el > PRIME_RANGE_BEFORE );

