
export default function ( a, b ) {
  if ( !a > b ) {
    [ a, b ] = [ b, a ]
  }

  let c = a % b;

  while ( c ) {
    let newC = b % c;
    b = c;
    c = newC;
  }

  return b;

}