
export default function( base, power, mod ) { // a ^ b mod n

  let res = 1; // Initialize result

  base = base % mod;  // Update x if it is more than or equal to p

  while ( power > 0 ) {
    // If power is odd, multiply x with result
    if ( power % 2 !== 0 )
      res = (res * base) % mod;

    power /= 2;
    power = Math.floor( power );
    base = (base * base) % mod;
  }

  return res;
}
