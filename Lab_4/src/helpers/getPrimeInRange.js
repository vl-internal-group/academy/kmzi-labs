export default function( n ) {
  if ( n < 2 ) {
    return [];
  }

  let lp = [],
    pr = [];

  for ( let i = 2; i < n; i++ ) {
    lp[ i ] = 0;
  }

  for ( let i = 2; i < n; i++ ) {
    if ( lp[ i ] === 0 ) {
      lp[ i ] = i;
      pr.push( i );
    }

    for ( let j = 0; pr[ j ] <= lp[ i ] && (i * pr[ j ]) <= n; j++ ) {
      lp[ i * pr[ j ] ] = pr[ j ];
    }
  }

  return pr;
}