import primeNumbers from './config/primeNumbers.js';
import euklidNZD from './helpers/euklidNZD.js';
import binPowMult from './helpers/binPowMult.js';
import chunkString from './helpers/chunkString.js';

export default class RSA {
  constructor(){}

  generateKeyPair( ) {

    // Pick two random prime nums
    let p1 = primeNumbers[ Math.ceil( Math.random() * primeNumbers.length - 1 )];
    let p2 = primeNumbers.filter( el => el !== p1 )[ Math.ceil( Math.random() * ( primeNumbers.length - 1 )) - 1 ];

    console.log( 'P1: ' , p1 );
    console.log( 'P2: ', p2 );

    let n = p1 * p2;

    console.log( 'n: ', n );

    let Fn = ( p1 - 1 ) * ( p2 - 1 );

    console.log( 'Fn: ', Fn );

    let e = primeNumbers[ Math.ceil( Math.random() * primeNumbers.length - 1 )];
    while( euklidNZD( e,  Fn ) !== 1 || e > Fn ) { // doesn't share factor with φ(n).
      e = primeNumbers[ Math.ceil( Math.random() * primeNumbers.length - 1 )];
    }
    console.log( 'e: ', e );

    let k = 1, d;
    do { // Ferma // Euklid
      d = ( k * Fn + 1 ) / e;
      k++;
    } while ( euklidNZD( e,  Fn ) !== 1 || ( e * d % n ) !== 1 || !Number.isInteger(d) );

    console.log( 'k: ', k );
    console.log( 'd: ', d );

    this.privateKey = { n, d };
    this.publicKey = { n, e };

    return [ JSON.stringify(this.publicKey), JSON.stringify( this.privateKey ) ];
  }

  static encrypt( value, publicKey ) {
    if ( !publicKey || !value ) {
      throw Error( "Not enough parameters provided for encryption" )
    }

    let key = JSON.parse( publicKey ); // n && e
    return chunkString( value, 3 ).map( el => binPowMult( parseInt( el ), key.e, key.n ) ).join(' ');
  }

  static decrypt( value, privateKey ) {
    if ( !privateKey || !value ) {
      throw Error( "Not enough parameters provided for encryption" )
    }

    let key = JSON.parse( privateKey ); // n && d
    return value.split(' ').map( el => binPowMult( parseInt( el ), key.d, key.n ) ).join('');
  }

}