import RSA from './RSA.js';

export default class Controller {

  constructor() {
    this.RSAcodding = new RSA();
  }

  getDOMValue( id ) {
    let doc = document.getElementById( id );
    return doc ? doc.value : null
  }

  setDOMValue( id, value ) {
    document.getElementById( id ).value = value
  }

  generateNewPair() {
    let [ publicKey, privateKey ] = this.RSAcodding.generateKeyPair();

    this.setDOMValue( "private-key", privateKey );
    this.setDOMValue( "public-key", publicKey );
  }

  encrypting() {
    try {
      let value = this.getDOMValue( "out-text" );
      let key = this.getDOMValue( "public-key-pc" );

      let result = RSA.encrypt( value, key );
      this.setDOMValue( "out-text", result );

    } catch ( e ) { console.error( e ) }
  }

  decrypting() {
    try {
      let value = this.getDOMValue( "in-text" );
      let key = this.getDOMValue( "private-key" );

      let result = RSA.decrypt( value, key );

      this.setDOMValue( "in-text", result );

    } catch ( e ) { console.error( e ) }
  }

}