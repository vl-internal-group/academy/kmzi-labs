import { Node } from './Node.mjs';

export class HuffmanCoding {
  constructor(){
    this.codeMap = {};
    this.charFrequency = {};
    this.codeTree = null;
  }

  encrypt( data ) {
    this.charFrequency  = this.getCharFrequency([...data]);
    this.codeTree = this.buildNewTree( this.charFrequency  );

    this.parseTreeLeaves( this.codeTree ); // will generate codeMap

    // show code map to console
    console.log( this.codeMap );

    this.entropy = this.getEntropy( data.length );
    console.log( 'Entropy >', this.entropy );

    this.price = this.getPrice( data.length );
    console.log( 'Price >', this.price );

    this.redudancy = this.price - this.entropy;
    console.log( 'Redundancy >', this.redudancy );

    return [...data].map( ch => this.codeMap[ch] ).join('');
  }

  getCharFrequency( charArray ) {
    let freqObj = {};

    for( const ch of charArray ) {
      if ( freqObj[ch] )
        freqObj[ch]++;
      else
        freqObj[ch] = 1;
    }

    return freqObj;
  }

  buildNewTree( charFrequency ){
    this.codeMap = {};

    let sortFreq = []; // transform to array of objects for sorting
    for (let ch in charFrequency) {
      sortFreq.push({ ch : ch, w : charFrequency[ch] });
    }
    sortFreq.sort((a, b) => a.w - b.w); // sort nodes

    let root = sortFreq[0]? new Node( sortFreq[0].w, sortFreq[0].ch ) : null;
    while ( sortFreq[0] && sortFreq[1] ) {
      root = new Node();

      root.left = sortFreq[0].root ? sortFreq[0].root : new Node( sortFreq[0].w, sortFreq[0].ch );
      root.right = sortFreq[1].root ? sortFreq[1].root : new Node( sortFreq[1].w, sortFreq[1].ch );

      let newWeight = sortFreq[0].w + sortFreq[1].w;

      sortFreq = sortFreq.splice( 2 );

      if ( sortFreq.length > 0 ) {
        let iter = 0;
        for ( iter; iter < sortFreq.length; iter++ ) {
          if ( sortFreq[ iter ].w >= newWeight ) {
            break;
          }
        }
        sortFreq.splice( iter, 0, { ch: '*', w: newWeight, root: root } );
      }

    }

    return root;
  }

  parseTreeLeaves( tree ) {
    this.leftBranchRecursive( tree );
  }

  leftBranchRecursive( root, code, acc = '') {
    if (!root) return;

    acc = code? acc + code : acc; // accumulate code

    // Check whether this node is a leaf node and is left.
    if (!root.left && !root.right ) {
      this.codeMap[ root.txt ] = acc ? acc : '';
      acc = '';
    }

    // Pass 0 for left and 1 for right
    this.leftBranchRecursive( root.left, '0', acc );
    this.leftBranchRecursive( root.right, '1', acc );
  }

  getEntropy( length ) {
    return -Object.values( this.charFrequency ).reduce(( acc, cur ) => acc + cur / length * Math.log2( cur / length ), 0);
  }

  getPrice( length ) {
    return Object.keys( this.charFrequency ).reduce(( acc, cur ) => acc + this.charFrequency[cur] / length * this.codeMap[cur].length, 0);
  }

  decrypt( data ) {
    try {

      if( this.codeTree ) {
        return this.decryptByTree([...data]);
      } else {
        throw Error('Cannot decoded without tree');
      }

    } catch ( e ) { console.error( e ); }
  }

  decryptByTree( charArray ) {
    let decodedText = '';
    let currentNode = this.codeTree;

    for( const ch of charArray ) {
      if ( ch === "0" ) {
        currentNode = currentNode ? currentNode.left : () =>{ throw Error('Wrong path for current tree') };
      }
      else if ( ch === "1" ) {
        currentNode = currentNode? currentNode.right : () =>{ throw Error('Wrong path for current tree') };
      }
      if ( !currentNode.left && !currentNode.right ) {
        decodedText += currentNode.txt;
        currentNode = this.codeTree;
      }
    }

    return decodedText;
  }

}

