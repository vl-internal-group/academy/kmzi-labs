
export class Node {
  constructor( weight = null, txt = 'node' ) {
    this.left = null;
    this.right = null;
    this.weight = weight;
    this.txt = txt;
  }
}
