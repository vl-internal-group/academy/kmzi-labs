import { HuffmanCoding } from './HuffmanCoding.mjs';

export class Controller {

  constructor() {
      this.huffmanCoding = new HuffmanCoding();
  }

  getDOMValue( id ) {
    return document.getElementById( id ).value
  }

  setDOMValue( id, value ) {
    document.getElementById( id ).value = value
  }

  encrypting() {
    this.setDOMValue( "out-text", this.huffmanCoding.encrypt( this.getDOMValue( "in-text" ) ));
  }

  decrypting() {
    this.setDOMValue( "in-text", this.huffmanCoding.decrypt( this.getDOMValue( "out-text" ) ));
  }

}