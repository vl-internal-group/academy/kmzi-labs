import Key from "./GOSTKey.js";
import converter from "./helpers/converter.js";
import gostTables from "./config/GOSTtables.js";
import chunkString from "./helpers/chunkString.js";

const SIZE_OF_BLOCK = 64;

const QUANTITY_OF_ROUNDS = 32;
const S_LEFT_SHIFT = 11;
const MODULO_POWER = Math.pow( 2, 32 );

// realization of GOST 28147-89 cipher algorithm
export default class GOST {
  constructor(){
    this.blocks = [];
  }

  encrypt( input, key ) {
    return this.gostFeistelOperation( input, key, true )
  }

  refactorInputLength( input ) { // pad zeroes for n * 64b message
    let rep = input.length % SIZE_OF_BLOCK;
    return input + '0'.repeat( rep && ( SIZE_OF_BLOCK - rep ));
  }

  cutTextIntoBlocks( input ) { // cut to 64b blocks
    let blocks = [];

    let blocksCount = input.length / SIZE_OF_BLOCK;
    let lengthOfBlock = input.length / blocksCount;

    for ( let i = 0; i < blocksCount; i++ ) {
      blocks.push( input.substr( i * lengthOfBlock, lengthOfBlock ))
    }

    return blocks;
  }

  XOR( s1, s2 ) {
    // you cannot just convert to int and ^ XOR it. js removes padding zeroes and the result has been changed
    return [...s1].reduce(( acc, cur, i ) => acc + ( s1[i] ^ s2[i] ), '');
  }

  modulo( b ) {
    // a = b (mod n).
    // a = kn + b.

    while ( b > MODULO_POWER ) {
      b -= MODULO_POWER;
    }

    return b;
  }

  performLeftShift( key, shift) {
    return key.substr( shift ) + key.slice(0, shift )
  }

  gostFeistelOperation( input, key, isEncryption ) {

    this.blocks = [];
    if ( !input ) {
      throw Error( "Not enough parameters provided for decryption" )
    }

    this.gostKey = new Key( key );

    this.gostKey.populateSubKeys(); // creates round keys 1 -> QUANTITY_OF_ROUNDS

    this.blocks = this.cutTextIntoBlocks( this.refactorInputLength( input ), false );

    let result = [];
    for ( const block of this.blocks ) {

      // 64 -> 2^32
      let subMessage = chunkString( block, SIZE_OF_BLOCK / 2 ); // Left - 0, Right - 1

      // processed through all rounds
      for ( let i = 0; i < QUANTITY_OF_ROUNDS; i++ ) {

        let roundKey = this.gostKey.roundKeys[ isEncryption ? i : QUANTITY_OF_ROUNDS - 1 - i ];

        // ( N + X ) mod 2^32
        let S = converter.num2bin( this.modulo( parseInt( subMessage[ 0 ], 2 ) + parseInt( roundKey, 2 ) ), 32 );

        // chunk S : 32 -> 8 * 4
        S = chunkString( S, 4 ).map(( Sm, m ) => converter.num2bin( gostTables.S[ m ][ parseInt( Sm, 2 ) ], 4 ));

        // S << 11
        S = this.performLeftShift( S.join(''), S_LEFT_SHIFT );

        // S (+) N1
        S = this.XOR( S, subMessage[ 1 ] );

        subMessage = [ S, subMessage[ 0 ]];

      }

      result.push( subMessage[1] + subMessage[0] );
    }

    return [ result.join(''), this.gostKey.key ];

  }

  decrypt( input, key ) {
    return this.gostFeistelOperation( input, key, false )
  }


}

