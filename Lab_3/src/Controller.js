import GOST from './GOST.js';

import converter from './helpers/converter.js';

export default class Controller {

  constructor() {
    this.GOSTcodding = new GOST();
  }

  getDOMValue( id ) {
    return document.getElementById( id ).value
  }

  setDOMValue( id, value ) {
    document.getElementById( id ).value = value
  }

  convertIfExist( value ) {
    return value ? converter.str2bin( value ) : value;
  }

  encrypting() {
    try {
      let value = this.getDOMValue( "in-text" );
      let key = this.getDOMValue( "key-enc" );

      let [ newValue, newKey ] = this.GOSTcodding.encrypt( this.convertIfExist(value), this.convertIfExist(key));
     
      this.setDOMValue( "out-text", converter.bin2str( newValue ));
      this.setDOMValue( "key-decr", converter.bin2str( newKey ));
      this.setDOMValue( "key-enc", converter.bin2str( newKey ));

    } catch ( e ) { console.error( e ) }
  }

  decrypting() {
    try {
      let value = this.getDOMValue( "out-text" );
      let key = this.getDOMValue( "key-decr" );

      let [ newValue, newKey ] = this.GOSTcodding.decrypt( converter.str2bin( value ) , this.convertIfExist( key ) );
      this.setDOMValue( "in-text", converter.bin2str( newValue ));
      this.setDOMValue( "key-decr", converter.bin2str( newKey ));

    } catch ( e ) { console.error( e ) }
  }

}