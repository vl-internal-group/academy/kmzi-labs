import chunkString from './chunkString.js';

const BASE_PATTERN = 8;

export default class Converter {
  constructor(){}

  static reduceBase( base, pattern = BASE_PATTERN ) {
    return ( acc, cur ) => {
      let c = cur.charCodeAt(0).toString( base );
      return acc + "0".repeat( pattern ? pattern - c.length : 0 ) + c };
  }

  static str2bin( str, pattern ) {
    return [...str].reduce( Converter.reduceBase( 2, pattern ), "")
  }

  static num2bin( num, pattern ) {
    let bin = num.toString(2);
    return '0'.repeat( pattern - bin.length ) + bin;
  }

  static bin2str ( binaryCode ) {
    return chunkString( binaryCode, 8 ).map( el => String.fromCharCode(parseInt(el, 2) )).join('');
  }
}