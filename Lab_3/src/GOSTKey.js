import chunkString from "./helpers/chunkString.js";

const SIZE_OF_KEY = 256;
const SUBKEY_SIZE = 32;

export default class Key {
  constructor( key ){
    this.key = key ? this.refactorKey( key , SIZE_OF_KEY ) : this.generateRandomKey();
    this.roundKeys = [];
  }

  generateRandomKey() {
    let key = [];
    for ( let i = 0; i < SIZE_OF_KEY; i++ ) {
      key.push( Math.random() > 0.5 ? '1' : '0' );
    }
    return key.join('');
  }

  populateSubKeys( ) {
    // 256 -> 32 x 8
    let subKeys = chunkString( this.key, SUBKEY_SIZE );
    this.roundKeys = [ ...subKeys, ...subKeys, ...subKeys, ...subKeys.reverse() ];
  }

  // cuts or pad key to 256b
  refactorKey( input,  lengthKey ) {
    return input.length > lengthKey ? input.substr( 0, lengthKey ) : "0".repeat( lengthKey - input.length ) + input;
  }
}